#[allow(non_snake_case)]
use super::*;
use assert_json_diff::*;
use serde_json::json;

#[test]
fn Class_parses_from_string() {
    assert_eq!("h-entry".parse(), Ok(Class::Known(KnownClass::Entry)));
    assert_eq!("entry".parse(), Ok(Class::Known(KnownClass::Entry)));
    assert_eq!("h-card".parse(), Ok(Class::Known(KnownClass::Card)));
    assert_eq!("card".parse(), Ok(Class::Known(KnownClass::Card)));
}

#[test]
fn Class_deserializes_from_json() {
    assert_eq!(
        serde_json::from_str::<Class>("\"h-entry\"").map_err(|e| e.to_string()),
        Ok(Class::Known(KnownClass::Entry))
    );
    assert_eq!(
        serde_json::from_str::<Class>("\"entry\"").map_err(|e| e.to_string()),
        Ok(Class::Known(KnownClass::Entry))
    );
}

#[test]
fn Class_parses_vendor_specific_from_string() {
    assert_eq!(
        "h-x-entry".parse(),
        Ok(Class::VendorSpecific("entry".to_string()))
    );
    assert_eq!(
        "h-x-card".parse(),
        Ok(Class::VendorSpecific("card".to_string()))
    );
    assert_eq!(
        "h-x-app".parse(),
        Ok(Class::VendorSpecific("app".to_string()))
    );
}

#[test]
fn Class_parses_vendor_specific_from_json_string() {
    assert_eq!(
        serde_json::from_str::<Class>("\"h-x-app\"").ok(),
        Some(Class::VendorSpecific("app".to_string()))
    );
}

#[test]
fn Class_parses_foreign_type_from_json_string() {
    assert_eq!(
        serde_json::from_str::<Class>("\"h-magic\"").ok(),
        Some(Class::Unrecognized("magic".to_owned()))
    );
}

#[test]
fn Class_parses_foreign_type_from_string() {
    assert_eq!(
        "h-magic".parse(),
        Ok(Class::Unrecognized("magic".to_string()))
    );
}

#[test]
fn Item_deserializes_from_json() {
    let the_id = "an-id".to_string();
    let mf2_json = serde_json::json!({
        "properties": {
            "like-of": [
                "http://indieweb.org/like",
                "http://indieweb.org/reply",
            ],
            "comment": [
                "http://indieweb.org/comment",
                "http://indieweb.org/comment",
                "http://indieweb.org/comment",
                {
                    "type": ["h-cite"],
                    "properties": {
                        "content": [{
                            "html": "<b>wow there</b>",
                            "value": "wow there"
                        }]
                    }
                },
                "http://indieweb.org/comment",
            ],
            "content": [
                "Well okay"
            ]
        },
        "id": the_id,
        "type": ["h-entry"],
    });

    let expected_item = Item::new(
        Rc::new(ParentRelationship::Root),
        vec![Class::Known(KnownClass::Entry)],
    );
    expected_item.borrow_mut().id = Some(the_id);
    expected_item.borrow_mut().r#type = vec![Class::Known(KnownClass::Entry)];
    expected_item.borrow_mut().properties = Rc::new(RefCell::new(
        vec![
            (
                "like-of".to_string(),
                vec![
                    PropertyValue::Url("http://indieweb.org/like".parse().unwrap()),
                    PropertyValue::Url("http://indieweb.org/reply".parse().unwrap()),
                ],
            ),
            (
                "comment".to_string(),
                vec![
                    PropertyValue::Url("http://indieweb.org/comment".parse().unwrap()),
                    PropertyValue::Url("http://indieweb.org/comment".parse().unwrap()),
                    PropertyValue::Url("http://indieweb.org/comment".parse().unwrap()),
                ],
            ),
            (
                "content".to_string(),
                vec![PropertyValue::Plain("Well okay".to_string())],
            ),
        ]
        .iter()
        .cloned()
        .collect(),
    ));

    let node = PropertyValue::Item(Rc::new(RefCell::new(Item {
        parent: Rc::new(ParentRelationship::Root),
        id: None,
        value: None,
        children: Vec::default(),
        r#type: vec![Class::Known(KnownClass::Cite)],
        properties: Rc::new(RefCell::new(
            [(
                "content".to_string(),
                vec![PropertyValue::Fragment(Fragment {
                    html: "<b>wow there</b>".to_string(),
                    value: "wow there".to_string(),
                    lang: None,
                })],
            )]
            .iter()
            .cloned()
            .collect::<Properties>(),
        )),
    })));

    if let Some(items) = expected_item
        .borrow_mut()
        .properties
        .borrow_mut()
        .get_mut(&"comment".to_owned())
    {
        items.push(node);
    }

    if let Some(items) = expected_item
        .borrow_mut()
        .properties
        .borrow_mut()
        .get_mut(&"comment".to_owned())
    {
        items.push(PropertyValue::Url(
            "http://indieweb.org/comment".parse().unwrap(),
        ))
    }

    assert_json_eq!(expected_item.deref().borrow().clone(), mf2_json);

    assert_eq!(
        serde_json::from_value::<Item>(json!(
                    {
                        "properties": {
                            "audience": [],
                            "category": [],
                            "channel": [
                                "all"
                            ],
                            "content": {
                                "html": "<p>well-here-we-go</p>"
                            },
                            "name": "magic-omg",
                            "post-status": [
                                "published"
                            ],
                            "published": [
                                "2022-02-12T23:22:27+00:00"
                            ],
                            "slug": [
                                "Gzg043ii"
                            ],
                            "syndication": [],
                            "updated": [
                                "2022-02-12T23:22:27+00:00"
                            ],
                            "url": [
                                "http://localhost:3112/Gzg043ii"
                            ],
                            "visibility": [
                                "public"
                            ]
                        },
                        "type": [
                            "h-entry"
                        ]
                    }

        ))
        .err()
        .map(|e| e.to_string()),
        None
    );
}

#[test]
fn Item_deserializes_with_from_json_based_on_fixture() {
    let expected_document_result: Result<Document, _> = serde_json::from_value(json!({
        "items": [
        {
            "type": ["h-entry"],
            "properties": {
                "name": ["This should imply a p-name"]
            }
        },
        {
            "type": ["h-entry"],
            "properties": {
                "content": ["This should not imply a p-name since it has an p-* property."]
            }
        },
        {
            "type": ["h-entry"],
            "properties": {
                "content": [{
                    "value": "This should not imply a p-name since it has an e-* property.",
                    "html": "<p>This should not imply a p-name since it has an e-* property.</p>"
                }]
            }
        },
        {
            "type": ["h-entry"],
            "children": [
            {
                "type": ["h-entry"],
                "properties": {
                    "url": ["/foo"]
                }
            }
            ],
            "properties": {
                "like-of": [{
                    "value": "http://microformats.org/",
                    "type": ["h-cite"],
                    "properties": {
                        "name": ["Microformats"],
                        "url": ["http://microformats.org/"]
                    }
                }]
            }
        }
        ],
        "rels": {},
        "rel-urls": {}
    }))
    .map_err(crate::Error::JSON);

    let document = expected_document_result.unwrap();
    assert_eq!(document.items.len(), 4);
    let last_item = document.items.last().map(|i| i.deref().borrow().clone());

    assert_ne!(last_item, None);

    let item = last_item.unwrap();
    assert_ne!(item.children, vec![]);
}

#[test]
fn Node_renders_list_of_strings_from_json() {
    let json_string = r#"[
            "microformats",
            "rust-lang"
        ]"#;
    let json_val_result: Result<NodeList, serde_json::Error> = serde_json::from_str(json_string);
    let expected_node: NodeList = vec![
        PropertyValue::Plain("microformats".to_string()),
        PropertyValue::Plain("rust-lang".to_string()),
    ];
    assert!(json_val_result.is_ok());
    let json_val = json_val_result.unwrap();
    assert_eq!(json_val, expected_node);
}

#[test]
fn Document_empty_serializes_from_json() {
    let document = Document::new(None);
    let item = document
        .borrow_mut()
        .create_child_item(&[Class::Known(KnownClass::Entry)]);
    item.borrow_mut().r#type = vec![Class::Known(KnownClass::Entry)];

    assert_json_eq!(
        json!({
            "items": [
            {
                "type": ["h-entry"],
                "properties": {},
            }
            ],
            "rels": {},
            "rel-urls": {}
        }),
        document.deref().clone()
    );
}

#[test]
fn Document_finds_property_name_of_item() {
    let document = serde_json::from_value::<Document>(json!({
            "items": [
            {
                "type": ["h-entry"],
                "properties": {
                    "color": ["jump"],
                    "guard": "kick",
                    "org": [
                        {
                            "type": ["h-card"],
                            "properties": {
                                "url": ["https://indieweb.org/test"]
                            }
                        }
                    ]
                },
            },
            {
                "type": ["h-cite"],
                "properties": {
                    "content": [{"html": "A wild <a href='https://indieweb.org/child-ref'>child ref</a> appears", "value": "A wild child ref appears"}]
                }
            }
            ],
            "rels": {},
            "rel-urls": {}
        }))
        .unwrap();

    assert_eq!(
        document
            .find_property_name_with_value(PropertyValue::Plain("jump".to_string()))
            .len(),
        1,
        "finds plain-text property"
    );

    assert_eq!(
        document
            .find_property_name_with_value(PropertyValue::Url(
                "https://indieweb.org/test".parse().unwrap()
            ))
            .len(),
        1,
        "finds a nested item referenced by its URL"
    );

    assert!(document
        .find_property_name_with_value(PropertyValue::Plain("never".to_string()))
        .is_empty());
}

#[test]
fn Item_properties_add() {
    let mut item = Item::default();
    item.append_property("magic", PropertyValue::Plain("add".to_string()));
    item.append_property("magic", PropertyValue::Plain("add".to_string()));

    assert_eq!(
        item.properties
            .deref()
            .borrow()
            .get("magic")
            .map(|v| v.len()),
        Some(2)
    );
}

#[test]
fn Item_properties_with_matching_value() {
    let mut item = Item::default();
    item.append_property("right", PropertyValue::Plain("add".to_string()));
    item.append_property("wrong", PropertyValue::Plain("sub".to_string()));

    assert_eq!(
        item.properties_with_matching_value(PropertyValue::Plain("add".to_string())),
        vec!["right".to_string()],
        "finds property value"
    );
}

#[test]
fn PropertyValue_kinds() {
    let u: Url = "https://indieweb.org".parse().unwrap();

    assert_eq!(
        serde_json::from_str(&format!("{:?}", u.to_string())).map_err(crate::Error::JSON),
        Ok(PropertyValue::Url(u))
    );

    assert_eq!(
        serde_json::from_str(
            r#"
          {
            "alt": "company logos",
            "value": "http://example.com/images/logo.gif"
          }
        "#
        )
        .map_err(crate::Error::JSON),
        Ok(PropertyValue::Image(Image {
            src: "http://example.com/images/logo.gif".parse().unwrap(),
            alt: "company logos".to_string(),
        }))
    );
}

#[test]
fn try_from_json_for_item() {
    use serde_json::json;
    use std::convert::TryFrom;
    assert_eq!(
        None,
        Item::try_from(json!({"type": ["h-entry"], "properties": {}})).err()
    );
}

#[test]
fn item_into_iter() {
    let item = serde_json::from_value::<Item>(json!({
        "type": ["h-event"],
        "properties": {
            "content": "when is it?"
        },
        "children": [
            {"type": ["h-entry"], "properties": {"content": "foo"}},
            {"type": ["h-entry"], "properties": {"content": "bar"}},
            {"type": ["h-entry"], "properties": {"content": "baz"}}
        ]
    }))
    .unwrap();

    assert_eq!(item.into_iter().count(), 4);
}

#[test]
fn document_into_iter() {
    let document = serde_json::from_value::<Document>(json!({
        "items": [
            {
                "type": ["h-entry"],
                "properties": {
                    "content": "I think therefore I post."
                },
            },
            {
                "type": ["h-cite"],
                "properties": {
                    "content": "what they said"
                }
            },
            {
                "type": ["h-card"],
                "properties": {
                    "content": "who is this?"
                }
            },
            {
                "type": ["h-event"],
                "properties": {
                    "content": "when is it?"
                },
                "children": [
                    {"type": ["h-entry"], "properties": {"content": "foo"}},
                    {"type": ["h-entry"], "properties": {"content": "bar"}},
                    {"type": ["h-entry"], "properties": {"content": "baz"}}
                ]
            }

        ],
        "rels": {},
        "rel-urls": {}
    }))
    .unwrap();

    assert_eq!(document.into_iter().count(), 7);
}
