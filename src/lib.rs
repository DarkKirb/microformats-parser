pub mod parse;
pub mod types;

pub use html5ever;
pub use url;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("The type {0:?} is not a known Microformats class supported by this library.")]
    NotKnownClass(String),

    #[error("An error ocurred when parsing IO.")]
    IO(#[from] std::io::Error),

    #[error("The encountered node was not an element")]
    NotAnElement,

    #[error("Invalid property class.")]
    InvalidPropertyClass,

    #[error("Parsing URL statement: {0}")]
    Url(#[from] url::ParseError),

    #[error("UTF8: {0}")]
    Utf8(#[from] std::string::FromUtf8Error),

    #[error("JSON: {0}")]
    JSON(#[from] serde_json::Error),

    #[error(transparent)]
    Relation(#[from] crate::parse::RelationFromElementError),

    #[error("Integer: {0}")]
    ParseIntError(#[from] std::num::ParseIntError),

    #[error("Temporal {0}")]
    Temporal(#[from] types::temporal::Error),

    #[error("Missing property {0:?} when converting from JSON.")]
    ObjectMissingProperty(String),

    #[error("The provided JSON value was not an object.")]
    NotAnObject,
}

impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

/// Parses the provided HTML into a `types::Document` resolved with the proviedd URL.
///
/// ```
/// # use parse::*;
/// # use types::*;
/// # use microformats::*;
///
/// let document = from_html(r#"
/// <html>
///     <head>
///         <link rel="author me" href="/author">
///     </head>
///     <body>
///     </body>
/// </html>
/// "#, "https://example.com".parse().unwrap());
///
/// assert!(document.is_ok());
///
/// let doc = document.unwrap();
/// ```
pub fn from_html(html: &str, url: url::Url) -> Result<types::Document, Error> {
    parse::Parser::new(html, url).and_then(|mut p| p.parse())
}

/// Parses the HTML stored in the provided reader into a `types::Document` resolved with the provided URL.
pub fn from_reader<R>(mut reader: R, url: url::Url) -> Result<types::Document, Error>
where
    R: std::io::Read,
{
    let mut html = String::with_capacity(8096);
    reader
        .read_to_string(&mut html)
        .map_err(Error::IO)
        .and_then(|_| from_html(&html, url))
}

/// Parses the HTML and returns the specified item, if found.
pub fn item_within(
    html: &str,
    resolving_url: url::Url,
    item_url: url::Url,
) -> Result<Option<types::PropertyValue>, Error> {
    from_html(html, resolving_url).map(|d| d.get_item_by_url(&item_url))
}

/// Removes all of the embebbed HTML and resolves any URLs found in it.
///
/// ```
/// # use microformats::*;
///
/// assert_eq!(
///     Ok("https://indieweb.org/logo the IndieWeb logo".to_string()),
///     resolve_text_from_html(r#"<img src="/logo">the IndieWeb logo"#,
///     "https://indieweb.org/why".parse().unwrap())
/// );
/// ```
pub fn resolve_text_from_html(html: &str, resolving_url: url::Url) -> Result<String, Error> {
    let dom = parse::Parser::fragment_from_html(html)?;
    let children = dom.document.children.borrow();

    children
        .iter()
        .try_fold(String::default(), |acc, node| {
            match parse::extract_text(std::rc::Rc::clone(&node), resolving_url.clone()) {
                Ok(newstr) => {
                    if !acc.is_empty() {
                        Ok(vec![acc, newstr].join(" "))
                    } else {
                        Ok(newstr)
                    }
                }
                Err(e) => Err(e),
            }
        })
        .map(|s| s.trim().to_string())
}

#[cfg(test)]
pub mod test;
