pub fn enable_logging() {
    let _ = env_logger::builder().is_test(true).try_init();
}

pub type FixtureData = (String, String);

use std::path::PathBuf;

use assert_json_diff::assert_json_include;
use url::Url;

use crate::{parse::Parser, types::Document};

fn read_fixture(p: &str) -> FixtureData {
    let base_path = format!(
        "{}/vendor/microformats-test/tests/{}",
        env!("CARGO_MANIFEST_DIR"),
        p
    );

    let pathbuf = PathBuf::from(base_path);

    let mut html_path = pathbuf.clone();
    html_path.set_extension("html");
    let mut json_path = pathbuf.clone();
    json_path.set_extension("json");

    let html = std::fs::read_to_string(html_path.clone())
        .expect(format!("Could not read the HTML from {:?}.", html_path).as_str());

    let json = std::fs::read_to_string(json_path.clone())
        .expect(format!("Could not read the JSON from {:?}.", json_path).as_str());

    (html, json)
}

fn check_fixture_for_parser(fixture_name: &str) {
    let the_url: Url = "http://example.com/".parse().unwrap();
    let (fixture_html, expected_mf2_json_string) = read_fixture(fixture_name);
    let expected_mf2_json_result = serde_json::from_str(&expected_mf2_json_string);

    let mut expected_document: Document = if let Ok(doc) = expected_mf2_json_result {
        doc
    } else {
        panic!(
            "Failed to generate document from the JSON {} because {:#?}",
            expected_mf2_json_string,
            expected_mf2_json_result.err()
        );
    };
    expected_document.url = Some(the_url.clone());
    log::trace!("The expected MF2 document is {:#?}", expected_document);

    let mut parser = Parser::new(&fixture_html, the_url.clone())
        .expect("The provided HTML could not be parsed.");
    let document_opt = parser.parse();

    assert!(document_opt.is_ok());
    let document = document_opt.unwrap();

    log::info!(
        "Presented MF2-JSON as the document {:#?}",
        expected_document
    );
    log::info!("Read MF2-JSON as {}", expected_mf2_json_string);
    log::info!(
        "Parsed MF2-HTML to be the document {}",
        serde_json::to_string_pretty(&document).unwrap()
    );

    assert_json_include!(actual: document, expected: expected_document);
}

mod generated;
